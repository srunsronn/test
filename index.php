<?php
    include "config.php";

    if(isset($_POST['add_the_product'])){
        $p_name = $_POST['p_name'];
        $p_price = $_POST['p_price'];
        $p_image = $_FILES['p_image']['name'];
        $tmp_file = $_FILES['p_image']['tmp_name'];
        $image_folder = 'uploadimg/'.$p_image;

        $sql = "INSERT INTO `products` (name, price, image) VALUES ('$p_name', '$p_price', '$p_image')";
        $insert_query = mysqli_query($conn, $sql);

        if($insert_query){
            move_uploaded_file($tmp_file, $image_folder); // move img file
            echo "<script>alert('Add Product Successfully!')</script>";
        }
        else{
            echo "<script>alert('Error: Add Product Failed!')</script>";
        }
        
    }

    // Delete Action
    if(isset($_GET['delete'])){
       $pro_id = $_GET['id'];
       $delete_query =mysqli_query($conn,"DELETE FROM products WHERE id = $pro_id");

       if($delete_query){
            echo "<script>alert('Product deleted!');</script>";
            header('location: index.php');
            exit;
       }
       else{
            echo "<script>alert('Product wasn't delete!');</script>";
            header('location: index.php'); 
            exit;
       }
    }

    // Edit Action


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
    <title>Shopping cart</title>
</head>
<body>
    <?php include "header.php"; ?>
    <br/>
    <br/>
    <br/>
    <div class="add_pro_section container d-flex justify-content-center align-items-center">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="border shadow rounded" style="background-color: #f1f1f1; padding: 20px;">
                <h3 class="text-center text-warning">ADD A NEW PRODUCT</h3>
                <input class="form-control mt-3 mb-3" type="text" name="p_name" placeholder="Enter product name..." required />
                <input class="form-control mb-3" type="text" name="p_price" placeholder="Enter product price..." required />
                <input class="form-control mb-3" type="file" name="p_image" accept="image/png, image/jpg, image/webp, image/jepg" required />
                <input class="btn btn-success form-control" type="submit" name="add_the_product" value="Add The Product" /> 
            </div>
        </form>
    </div>
    <div class="display_pro_section container">
        <h3>Order Details</h3>
        <div class="table-responsive">
            <table class="table text-center">
                <tr>
                    <th>Product Image</th>
                    <th>Product Name</th>
                    <th>Product Price</th>
                    <th>Action</th>
                </tr>
                <?php 
                    $select_query = mysqli_query($conn, "SELECT * FROM products");
                    if(mysqli_num_rows($select_query) > 0){ 
                        while ($row = mysqli_fetch_assoc($select_query)) {
                ?>
                <tr>
                    <td><img style="width: 100px;" src="uploadimg/<?php echo $row['image'] ?>"></td>
                    <td class="pt-5"><?php echo $row['name'] ?></td>
                    <td class="pt-5 text-danger">$ <?php echo $row['price'] ?>/-</td>
                    <td class="pt-3">
                        <a href="index.php?delete&id=<?php echo $row['id'] ?>" class="btn btn-danger w-50 mb-2"><i class="fa-solid fa-trash"></i> Delete</a><br/>
                        <a href="edit_admin.php?edit&id=<?php echo $row['id'] ?>" class="btn btn-warning w-50"><i class="fa-solid fa-pencil"></i> Edit</a><br/>
                    </td>    
                </tr>
                <?php
                        }
                    }
                ?>
            </table>
        </div>
    </div>
</body>
</html>