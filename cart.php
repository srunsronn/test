<?php
include 'config.php';

    if(isset($_POST['update_cart'])){
        $cart_id = $_POST['hidden_cart_id'];
        $update_qty = $_POST['update_qty'];

        $update_cart = mysqli_query($conn, "UPDATE cart SET qautity=$update_qty WHERE id=$cart_id");
        
        if($update_cart){
            echo "<script>alert('Updated successful!');</script>";
            header('location: cart.php');
        }else{
            echo "<script>alert('Update Not success!');</script>";
            header('location: cart.php');
        }
    }
    if(isset($_GET['remove'])){
        $delete_id = $_GET['id'];

        $delete_cart = mysqli_query($conn, "DELETE FROM cart WHERE id=$delete_id");
        if($delete_cart){
            echo "<script>alert('Deleted successful!');</script>";
            header('location: cart.php');
        }else{
            echo "<script>alert('Deleted Not successful!');</script>";
            header('location: cart.php');
        }
    }
    if(isset($_GET['remove_all'])){

        $delete_all_cart = mysqli_query($conn, "DELETE FROM cart");
        header('location: cart.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Cart</title>
</head>
<body>
    <?php include "header.php"; ?>
    <div class="container">
        <h1 class="mt-5">Shopping Cart</h1>
        <table class="table text-center mt-3">
            <tr>
                <th>Image</th>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total Price</th>
                <th>Action</th>
            </tr>
            <?php 
                $cart_select = mysqli_query($conn, "SELECT * FROM cart");
                $grand_price = 0;
                if(mysqli_num_rows($cart_select) > 0){
                    while ($fetch_cart = mysqli_fetch_assoc($cart_select)){
                    
            ?>
            <tr>
                <td><img style="height: 100px;" src="uploadimg/<?php echo $fetch_cart['image'] ?>" alt=""></td>
                <td class="pt-5"><?php echo $fetch_cart['name'] ?></td>
                <td class="pt-5 text-danger">$ <?php echo $fetch_cart['price'] ?>/-</td>
                <td>
                    <form action="" method="post" style="margin-top: 40px;">
                        <input type="hidden" name="hidden_cart_id" value="<?php echo $fetch_cart['id'] ?>">
                        <input type="number" min="1" name="update_qty" value="<?php echo $fetch_cart['qautity'] ?>">
                        <input class="btn btn-warning" type="submit" name="update_cart" value="Update">      
                    </form>
                </td>
                <td class="pt-5">
                    $ <?php echo $total_price = number_format($fetch_cart['price'] * $fetch_cart['qautity']) ?>
                </td>
                <td>
                    <a class="btn btn-danger mt-4" href="cart.php?remove&id=<?php echo $fetch_cart['id'] ?>">
                    <i class="fa-solid fa-trash"></i>&nbsp;Remove</a>
                </td>
            </tr>
            <?php 
                $grand_price += $total_price;
                    };
                }; 
            ?>
            <tr>
                <td><a class="btn btn-warning mt-2" href="view_product.php">Continue-Shopping</a></td>
                <td colspan="3" class="pt-4">
                    Grand Total
                </td>
                <td class="pt-4 text-danger">$ <?php echo $grand_price;?></td>
                <td class="mt-3">
                    <a class="btn btn-danger mt-2" href="cart.php?remove_all">
                    <i class="fa-solid fa-trash"></i>&nbsp;Remove All</a>
                </td>
            </tr>
        </table>
        
    </div>
</body>
</html>