<?php
include "config.php";

    if(isset($_POST['add_to_cart'])){
        $product_img = $_POST['product_img'];
        $product_name = $_POST['product_name'];
        $product_price = $_POST['product_price'];
        $product_qty = 1;

        $select_cart =mysqli_query ($conn, "SELECT * FROM `cart` WHERE name = '$product_name'");
        if(mysqli_num_rows($select_cart) > 0){
            echo "<script>alert('Product is already added to the cart!')</script>";
        }else{
            $insert_product = mysqli_query($conn, "INSERT INTO cart (name, price, image, qautity) VALUES ('$product_name',  '$product_price', '$product_img', '$product_qty')");
            echo "<script>alert('Product added to the cart!')</script>";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>Document</title>
</head>
<body>
    <?php include 'header.php'; ?>
    <div class="container">
        <h3 class="text-center mt-5" style="font-size: 50px;">Lasted Products</h3>

        <div class="row d-flex">
            <?php
            $select_product = mysqli_query($conn, 'SELECT * FROM products');
            if(mysqli_num_rows($select_product) > 0){
                while ($fetch_pro = mysqli_fetch_assoc($select_product)){
            ?>
            <div class="col-md-4 shadow bordered p-3 g-2 mb-3 text-center">
                <form action="" method="post">
                    <img src="uploadimg/<?php echo $fetch_pro['image'] ?>" alt="" width="200px">
                    <h3><?php echo $fetch_pro['name']?></h3>
                    <h4 class="text-danger">$ <?php echo $fetch_pro['price']?></h4>
                    <input type="hidden" name="product_img" value="<?php echo $fetch_pro['image'] ?>">
                    <input type="hidden" name="product_name" value="<?php echo $fetch_pro['name'] ?>">
                    <input type="hidden" name="product_price" value="<?php echo $fetch_pro['price'] ?>">
                    <input type="submit" name="add_to_cart" value="Add To Cart" class="btn btn-info form-control"/>
                </form>
            </div>
            <?php
                }
            }
            ?>
        </div>
    </div>
</body>
</html>