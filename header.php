<nav class="navigation_bar bg-info d-flex justify-content-between p-3" style="height: 80px;">
    <div class="nav-brand text-light " style="padding-left: 10rem; padding-top: 8px;">
        <h3>FOODIES</h3>
    </div>
    <div class="nav-item">
        <ul class="d-flex" style="padding-right: 10rem; padding-top: 8px;">
            <li class="nav-link mx-3"><a style="text-decoration: none; font-size: 20px;" href="index.php">Products</a></li>
            <li class="nav-link mx-3"><a style="text-decoration: none; font-size: 20px;" href="view_product.php">Show Products</a></li>
            <?php 
                $select_row = mysqli_query($conn, "SELECT * FROM cart");
                $count_row = mysqli_num_rows($select_row);
            ?>
            <li class="nav-link mx-3">
                <a style="text-decoration: none; font-size: 20px;" href="cart.php">
                    Cart <span class="bg-white text-danger rounded" style="font-size: 20px;"><?php echo $count_row ?></span>
                </a>
            </li>
        </ul>
    </div>
</nav>