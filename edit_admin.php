<?php 
    include "config.php";

    if(isset($_POST['edit_the_product'])){
        $p_edit_id = $_POST['p_edit_id'];
        $p_edit_name = $_POST['p_edit_name'];
        $p_edit_price = $_POST['p_edit_price'];
        $p_edit_image = $_FILES['p_edit_image']['name'];
        $edit_tmp_file = $_FILES['p_edit_image']['tmp_name'];
        $edit_image_folder = 'uploadimg/'.$p_edit_image;
        
        $edit_query = mysqli_query($conn, "UPDATE products SET name='$p_edit_name', price=' $p_edit_price', image='$p_edit_image' WHERE id = '$p_edit_id'") or die("Can't Connecting Database!");
        if($edit_query){
            move_uploaded_file( $edit_tmp_file, $edit_image_folder);
            echo "<script>alert('Updated Product Successful!');</script>";
            header("Location: index.php");
           
        }
        else{
            echo "<script>alert('Can't Update Product!');</script>";
            header("Location: index.php");
           
        }
    }
    if(isset($_POST['cancel_edit'])){
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  </head>
    <title>Edit Product</title>
</head>
<body>
    <br/>
    <br/>
    <br/>
    <div class="edit_pro_section container d-flex justify-content-center align-items-center">
        <?php 
            if(isset($_GET['edit'])){
                $edit_id = $_GET['id'];
                $edit_query = mysqli_query($conn, "SELECT * FROM products WHERE id = $edit_id");
                if(mysqli_num_rows($edit_query) > 0){
                    while ($fetch_edit = mysqli_fetch_assoc($edit_query)) { ?>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="border shadow rounded" style="background-color: #f1f1f1; padding: 20px;">
                        <h3 class="text-center text-warning">EDIT THE PRODUCT</h3>
                        <img style="height: 200px; margin-left: 3rem;" src="uploadimg/<?php echo $fetch_edit['image'] ?>" alt="">
                        <input type="hidden" name="p_edit_id" value="<?php echo $fetch_edit['id'] ?>">
                        <input class="form-control mt-3 mb-3" type="text" name="p_edit_name" value="<?php echo $fetch_edit['name'] ?>" required />
                        <input class="form-control mb-3" type="text" name="p_edit_price" value="$ <?php echo $fetch_edit['price'] ?>" required />
                        <input class="form-control mb-3" type="file" name="p_edit_image" accept="image/png, image/jpg, image/webp, image/jepg" required />
                        <input class="btn btn-info form-control mb-3" type="submit" name="edit_the_product" value="Edit The Product" /> 
                        <a href="index.php" class="btn btn-warning form-control">Cancel</a> 
                    </div>
                </form>
                <?php
                    }
               }
            }
        ?>
        
    </div>